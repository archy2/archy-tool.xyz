#!/usr/bin/env bash

sudo pacman -S --needed --noconfirm git make fzf alacritty jq

cd /opt/archy-tool
sudo make uninstall
cd /opt/
sudo rm -rf /opt/archy-tool

sudo git clone https://gitlab.com/archy2/archy-tool.git
cd /opt/archy-tool
sudo make install

cd ~/

clear

#!/usr/bin/env bash

#install dependencies, alacritty needed so Archy can run on top of it
sudo pacman -S --needed --noconfirm git make fzf alacritty jq

cd /opt/
sudo git clone https://gitlab.com/archy2/archy-tool.git
cd archy-tool
sudo make install

clear
